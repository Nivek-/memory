// Fonction pour obtenir un nombre aléatoire compris entre min et max
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Tableau avec tous les td contenant la classe tray
var tray = document.getElementsByClassName("tray");

//Tableau contenant toutes les cartes du jeu en double
var cards = [
    "10_coeur.png",
    "10_coeur.png",
    "10_pique.png",
    "10_pique.png",
    "as_coeur.png",
    "as_coeur.png",
    "as_pique.png",
    "as_pique.png",
    "reine_coeur.png",
    "reine_coeur.png",
    "reine_pique.png",
    "reine_pique.png",
    "roi_coeur.png",
    "roi_coeur.png",
    "roi_pique.png",
    "roi_pique.png",
    "valet_coeur.png",
    "valet_coeur.png",
    "valet_pique.png",
    "valet_pique.png"
];

//Positionnement aléatoire des cartes en début de partie

// Pour chaque élément du tableau tray, une nouvelle balise image lui est ajoutée contenant une image alétatoire du tableau cards
for (var i = 0; i < (tray.length); i++) {
    var rand = getRandomIntInclusive(0, (cards.length - 1))
    var new_img = document.createElement("img");
    new_img.className = "face";
    new_img.src = "img/" + cards[rand];
    document.getElementsByTagName("td")[i].appendChild(new_img);
    //Retire la carte qui vient d'être positionnée du tableau cards pour qu'elle ne puisse plus revenir
    cards.splice(rand, 1);
};


var clickedCards = [];
var endGame = 0;
var score = 0;

//  Ne prend pas en compte le clic d'un utilisateur si 2 cartes différentes ne sont pas encore remises face verso
if (clickedCards.length < 2) {

    // A chaque clic sur un élément contenant la classe tray
    $('.tray').on('click', function () {
        // Récupère l'id de l'élément cliqué et lui retire 1 pour intéragir avec les tableaux
        var j = this.id - 1;
        // Ajout de l'id -1 de l'élément cliqué dans le tableau de comparaison
        clickedCards.push(j);
        // Tableau contenant toutes les classes ayant la class back
        var back = document.getElementsByClassName("back");
        // Tableau contenant toutes les classes ayant la class face
        var face = document.getElementsByClassName("face");

        // Si c'est la première carte cliquée, elle est retournée face recto
        if (clickedCards.length < 2) {
            back[j].style.display = 'none';
            face[j].style.display = 'block';

        // Empêche l'utilisateur de cliquer une deuxième fois sur la même carte    
        } else if (clickedCards.length == 2 && clickedCards[0] == clickedCards[1]) {
            clickedCards.pop();

        // Si c'est la deuxième carte cliquée, elle est retournée face recto et on augmente le score de 1 pour montrer le nombre de coups en fin de partie
        } else if (clickedCards.length == 2) {
            back[j].style.display = 'none';
            face[j].style.display = 'block';
            score++;

            // Les src des deux cartes retournées sont comparés grâce aux deux valeurs ajoutées dans le tableau clickedCards
            function testTime() {
                if (face[clickedCards[0]].src != face[clickedCards[1]].src) {
                    // Les deux cartes retournent face cachée si elles sont différentes
                    face[clickedCards[0]].style.display = "none";
                    face[clickedCards[1]].style.display = "none";
                    back[clickedCards[0]].style.display = "block";
                    back[clickedCards[1]].style.display = "block";
                } else {
                    // Les deux cartes identiques restent face visible et on leur ajoute la classe even afin qu'elles ne soient plus cliquables
                    tray[clickedCards[0]].classList.add('even');
                    tray[clickedCards[1]].classList.add('even');

                    // endGame est augmenté de 2 et quand il est égal au nombre de cartes sur le plateau la fin de partie est annoncée avec le score
                    endGame += 2;
                    if (endGame == tray.length) {
                        alert('Bravo vous avez gagné en ' + score + ' coups !')
                    };
                }
            }

            // Remets le tableau clickedCards à 0 toutes les 2 cartes pour pouvoir comparer les deux suivantes
            function eraseTab() {
                clickedCards.splice(0, clickedCards.length);
            }

            // Décalage de l'application des fonctions quand 2 cartes sont cliquées pour laisser le temps à l'utilisateur de voir les cartes
            setTimeout(testTime, 1000);
            setTimeout(eraseTab, 1001);
        }
    })
}